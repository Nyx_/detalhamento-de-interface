import netifaces


class Ifacedetails:
    def __init__(self):
        pass

    def get_interfaces(self):
        return netifaces.interfaces()

    def get_gateways(self):
        gateways_dic = {}
        gateways = netifaces.gateways()

        for gateway in gateways:
            try:
                gateway_iface = gateways[gateway][netifaces.AF_INET]
                gateway_ip = gateway_iface[0]
                iface = gateway_iface[1]
                gateways_list = [gateway_ip, iface]
                gateways_dic[gateway] = gateways_list
            except:
                pass

        return gateways_dic

    def get_address(self, interface):
        address = netifaces.ifaddresses(interface)
        link_address = address[netifaces.AF_LINK]
        iface_address = address[netifaces.AF_INET]
        iface_dic = link_address[0]
        link_dic = link_address[0]
        hardware_address = link_dic.get('addr')
        iface_addr = iface_dic.get('addr')
        iface_broadcast = iface_dic.get('broadcast')
        iface_netmask = iface_dic.get('netmask')

        return hardware_address, iface_addr, iface_broadcast, iface_netmask

    def get_networks(self, gateways_dic):
        networks_dic = {}

        for key, value in gateways.items():
            gateway_ip = value[0]
            iface = value[1]
            hardware_address, address, broadcast, netmask = self.get_address(iface)
            network = {
                'gateway': gateway_ip,
                'hardware addrress': hardware_address,
                'address': address,
                'broadcast': broadcast,
                'netmask': netmask
            }

            networks_dic[iface] = network



ifdt = Ifacedetails()
gateways = ifdt.get_gateways()
network_ifaces = ifdt.get_networks(gateways)

print(gateways, network_ifaces)